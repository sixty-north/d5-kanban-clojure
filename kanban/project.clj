(defproject kanban "0.1.0-SNAPSHOT"
  :description "Clojure implementation of the D5 kanban example."
  :url "https://bitbucket.org/sixty-north/d5-kanban-clojure"
  :license {:name "MIT"
            :url "http://opensource.org/licenses/MIT"}
  :dependencies [[clj-time "0.7.0"]
                 [org.clojure/clojure "1.6.0"]
                 [org.clojure/data.json "0.2.4"]
                 [org.clojure/tools.namespace "0.2.4"]
                 [org.clojure/tools.trace "0.7.5"]]
  :main infrastructure.core)
                 
