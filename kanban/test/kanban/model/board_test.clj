(ns kanban.model.board-test
  (:require [clojure.test :refer :all]
            [kanban.model.board :refer :all]
            [kanban.model.domain_events :refer :all]
            [kanban.model.entity :refer :all]
            [kanban.model.test-util :refer :all]))

; TODO: This needs a test that the message was actually generated,
; too. This only tests for structure when it is created.
(deftest start-project-generates-message
  (testing "start-project generates the correct message."
    (let [name "event"
          description "description"
          msg-handler (fn [e]
                        (is (= (:name (:data e)) name))
                        (is (= (:description (:data e)) description)))]
      (with-subscribe msg-handler
        (start-project name description)))))

(deftest start-project-creates-board
  (testing "start-project creates a board correctly."
    (let [new-id "1234567890"
          name "event"
          description "description"
          event (create-board-event :project-started
                                    new-id
                                    {:board-id new-id
                                     :name name
                                     :description description})
          board @(process-event event)]
      (is (= (:entity-id board) new-id))
      (is (= (:name board) name))
      (is (= (:description board) description)))))

(deftest discard-board-generates-message
  (testing "discard-board generates the correct message."
    (let [board (default-board)
          board-id (:entity-id @board)
          msg-handler (fn [e]
                        (is (= (:root-id e) board-id))
                        (is (= (:event-type e) :board-discarded)))]
      (with-subscribe msg-handler
        (discard-board board)))))

(deftest discard-board-deletes-board
  (testing "discard-board deletes the board."
    (let [board (default-board)
          event (create-board-event :board-discarded
                                    (:entity-id @board))
          board (process-event event)]
      (is (nil? @board)))))

(deftest rename-board-publishes-event
  (testing "rename-board publishes event"
    (let [board (default-board)
          new-name "new name"
          msg-handler (fn [e]
                        (is (= (:name (:data e)) new-name))
                        (is (= (:root-id e) (:entity-id @board))))]
      (with-subscribe msg-handler
        (rename-board board new-name)))))

(deftest board-renamed-renames-board
  (testing "The board-renamed event renames a board."
    (let [board (default-board)
          new-name "new name"
          event (create-board-event :board-renamed
                                    (:entity-id @board)
                                    {:name new-name})
          board (process-event event board)]
      (is (= (:name @board) new-name)))))

(deftest change-board-description-publishes-event
  (testing "change-board-description publishes event"
    (let [board (default-board)
          new-description "new description"
          msg-handler (fn [e]
                        (is (= (:description (:data e)) new-description))
                        (is (= (:root-id e) (:entity-id @board))))]
      (with-subscribe msg-handler
        (change-board-description board new-description)))))

(deftest board-description-changed-changes-board-description
  (testing "The board-description-changed event changes the board's description."
    (let [board (default-board)
          new-description "new desc"
          event (create-board-event :board-description-changed
                                    (:entity-id @board)
                                    {:description new-description})
          board (process-event event board)]
      (is (= (:description @board) new-description)))))

