(ns kanban.model.test-util
  (:require [kanban.model.board :refer :all]
            [kanban.model.domain_events :only [subscribe unsubscribe]]))

(def default-board-name "test-board")
(def default-board-description "The default testing board.")

(defn default-board []
  (start-project default-board-name
                 default-board-description))

(defmacro with-subscribe [handler & body]
  `(let [handler# ~handler]
     (try
       (kanban.model.domain_events/subscribe handler#)
       ~@body
       (finally (kanban.model.domain_events/unsubscribe handler#)))))
