(ns kanban.model.work-item-test
  (:require [clojure.test :refer :all]
            [kanban.model.work-item :refer :all]
            [kanban.model.domain_events :refer :all]
            [kanban.model.entity :refer :all]
            [kanban.model.test-util :refer :all]
            [clj-time.core :as ctime]))

(deftest register-new-work-item-generates-message
  (testing "register-new-work-item generates the correct message."
    (let [name "witem"
          due-date (ctime/now)
          content "some content"
          handler (fn [e]
                    (let [data (:data e)]
                      (is (= (:name data) name))
                      (is (= (:due-date data) due-date))
                      (is (= (:content data) content))))]
      (with-subscribe handler
        (register-new-work-item name
                                :due-date due-date
                                :content content)))))

(deftest register-new-work-item-creates-entity
  (testing "register-new-work-item creates the right entity."
    (let [name "name"
          due-date (ctime/now)
          content "content"
          witem @(register-new-work-item name
                                         :due-date due-date
                                         :content content)]
      (is (= (:name witem) name))
      (is (= (:due-date witem) due-date))
      (is (= (:content witem) content)))))

(deftest register-new-work-item-throws-on-empty-name
  (testing "register-new-work-item throws on empty name."
    (is (thrown? IllegalArgumentException
                 (register-new-work-item "")))))

(deftest rename-work-item-generates-message
  (testing "rename-work-item generates correct message."
    (let [new-name "new name"
          witem (register-new-work-item "old name")
          handler (fn [e] (is (= (:name (:data e)) new-name)))]
      (with-subscribe handler
        (rename-work-item witem new-name)))))

(deftest rename-work-item-renames-work-item
  (testing "rename-work-item renames the work item."
    (let [new-name "new name"
          witem (-> (register-new-work-item "old name")
                    (rename-work-item new-name))]
      (is (= (:name @witem) new-name)))))

(deftest rename-work-item-throws-on-empty-name
  (testing "rename-work-item throws when name is empty."
    (let [witem @(register-new-work-item "name")]
      (is (thrown? IllegalArgumentException
                   (rename-work-item (:entity-id witem) ""))))))

(deftest set-work-item-due-date-generates-message
  (testing "set-work-item-due-date generates the right message."
    (let [work-item (register-new-work-item "item")
          date (ctime/now)
          handler (fn [e]
                    (is (= (:date (:data e)) date)))]
      (with-subscribe handler
        (set-work-item-due-date work-item date)))))

(deftest set-work-item-due-date-sets-date
  (testing "set-work-item-due-date sets the date."
    (let [work-item (register-new-work-item "item")
          date (ctime/now)]
      (set-work-item-due-date work-item date)
      (is (= (:due-date @work-item) date)))))

(deftest set-work-item-content-generates-message
  (testing "set-work-item-content generates the right message."
    (let [work-item (register-new-work-item "item")
          content "content"
          handler (fn [e]
                    (is (= (:content (:data e)) content)))]
      (with-subscribe handler
        (set-work-item-content work-item content)))))

(deftest set-work-item-content-sets-content
  (testing "set-work-item-content sets the content."
    (let [work-item (register-new-work-item "item")
          content "content"]
      (set-work-item-content work-item content)
      (is (= (:content @work-item) content)))))

