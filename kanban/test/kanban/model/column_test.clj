(ns kanban.model.column-test
  (:require [clojure.test :refer :all]
            [kanban.model.board :refer :all]
            [kanban.model.domain_events :refer :all]
            [kanban.model.test-util :refer :all]
            [utilities.data_structures :refer :all])
  (:use [kanban.model.work-item :only [register-new-work-item]]))

(deftest add-column-generates-message
  (testing "add-columns generates the correct message"
    (let [column-name "column"
          wip-limit 5
          board (start-project "" "")
          msg-handler (fn [e]
                        (is (= (:name (:data e)) column-name))
                        (is (= (:wip-limit (:data e)) wip-limit)))]
      (with-subscribe msg-handler
        (add-column board column-name wip-limit)))))

(deftest add-column-updates-board
  (testing "add-column updates a board correctly."
    (let [column-name "column"
          wip-limit 42
          board @(-> (start-project "" "")
                     (add-column column-name wip-limit))
          column (first (:columns board))]
      (is (= (count (:columns board)) 1))
      (is (= (:name column) column-name))
      (is (= (:wip-limit column) wip-limit))
      (is (= (count (:cards column)) 0))
      (is (= (:board-id column) (:entity-id board))))))

(deftest add-column-wip-limit-must-be-non-negative
  (testing "wip-limit must be non-negative for add-column."
    (is (thrown? Exception 
                 (add-column (:entity-id @(start-project "" ""))
                             "invalid column"
                             -1)))))

(deftest insert-column-generates-message
  (testing "insert-column generates the correct message"
    (let [column-name "column"
          wip-limit 5
          index 0
          board (start-project "" "")
          msg-handler (fn [e]
                        (is (= (:name (:data e)) column-name))
                        (is (= (:wip-limit (:data e)) wip-limit))
                        (is (= (:index (:data e)) index)))]
      (with-subscribe msg-handler
        (insert-column board index column-name wip-limit)))))

(deftest insert-column-updates-board
  (testing "insert-column updates a board correctly."
    (let [index 1
          name "inserted"
          wip-limit 42
          board @(-> (start-project "" "")
                     (add-column "col1" 5)
                     (add-column "col2" 10)
                     (insert-column index name wip-limit))
          column (nth (:columns board) index)]
      (is (= (count (:columns board)) 3))
      (is (= (:name column) name))
      (is (= (:wip-limit column) wip-limit))
      (is (= (:board-id column) (:entity-id board))))))

(deftest insert-column-wip-limit-must-be-non-negative
  (testing "wip-limit must be non-negative for insert-column."
    (is (thrown? IllegalArgumentException
                 (insert-column (start-project "" "")
                                0
                                "invalid column"
                                -1)))))

(deftest insert-column-at-end-of-range
  (testing "Columns can be inserted at end of range."
    (let [board (start-project "" "")]
      (insert-column board 0 "ok" 0))))

(deftest insert-column-index-within-range
  (testing "insert-column raises an exception when the index is out of range."
    (let [board (start-project "" "")]
      (is (thrown? IndexOutOfBoundsException
                   (insert-column board 1 "fail" 0))))))

(deftest remove-column-generates-message
  (testing "remove-column generates the correct message"
    (let [column-name "column"
          wip-limit 5
          board (-> (start-project "" "")
                    (add-column column-name wip-limit))
          column (column-with-name board column-name)
          msg-handler (fn [e] (is (= (:column-id (:data e)) (:entity-id column))))]
      (with-subscribe msg-handler
        (remove-column board (:entity-id column))))))

(deftest remove-column-updates-board
  (testing "remove-column updates a board correctly."
    (let [name "col2"
          wip-limit 42
          board (-> (start-project "" "")
                    (add-column "col1" 5)
                    (add-column name wip-limit))
          column (column-with-name board name)]
      (is (= (count (:columns @board)) 2))
      
      (let [board (remove-column board (:entity-id column))
            column (nth (:columns @board) 0)]
        (is (= (count (:columns @board)) 1))))))

(deftest column-with-name-give-right-column
  (testing "column-with-name returns the correct column"
    (let [board (reduce #(add-column %1 (format "col%s" %2) %2)
                        (start-project "" "")
                        (range 3))
          col (column-with-name board "col1")]
      (is (= (:name col) "col1"))
      (is (= (:wip-limit col) 1)))))

(deftest column-with-name-returns-nil
  (testing "column-with-name returns nil on no match"
    (is (= (column-with-name (start-project "" "") "nope") nil))))

(deftest number-of-work-items-works
  (testing "does number-of-work-items return the right value?"
    (let [wi-count 4
          col-name "column"
          board (-> (default-board)
                    (add-column col-name 10))
          column-id (:entity-id (column-with-name board col-name))
          board (reduce #(schedule-work-item
                           %1
                           column-id
                           (:entity-id @(register-new-work-item
                                         (str "work-item" %2))))
                         board
                         (range wi-count))]
      (is (= (number-of-work-items (column-with-id board column-id)) wi-count)))))

(deftest can-accept-work-item-allows-items
  (testing "can-accept-work-item returns true when there is space."
    (let [col-name "column"
          board (-> (default-board)
                    (add-column col-name 10))
          column (column-with-name board col-name)]
      (is (can-accept-work-item? column)))))

(deftest can-accept-work-item-disallows-items
  (testing "can-accept-work-item returns false when there is no space."
    (let [col-name "column"
          board (-> (default-board)
                    (add-column col-name 0))
          column (column-with-name board col-name)]
      (is (not (can-accept-work-item? column))))))

(deftest schedule-work-item-generates-message
  (testing "schedule-work-item generates the right message."
    (let [col-name "column"
          board (-> (default-board)
                    (add-column col-name 10))
          column (column-with-name board col-name)
          work-item (register-new-work-item "work-item")
          handler (fn [{data :data}]
                    (is (= (:work-item-id data) (:entity-id work-item)))
                    (is (= (:column-id data) (:entity-id column))))]
      (schedule-work-item board
                          (:entity-id column)
                          (:entity-id @work-item)))))

(deftest schedule-work-item-schedules-item
  (testing "schedule-work-item schedules an item"
    (let [col-name "column"
          board (-> (default-board)
                    (add-column col-name 10))
          work-item (register-new-work-item "work-item")
          column (column-with-name board col-name)]
      (is (not (in? (:cards column) (:entity-id @work-item))))
      
      (let [board (schedule-work-item
                   board
                   (:entity-id column)
                   (:entity-id @work-item))
            column (column-with-name board col-name)]
        (is (in? (:cards column)
                 (:entity-id @work-item)))))))

(deftest schedule-work-item-throws
  (testing "schedule-work-item throws when wip-limit exceeded."
    (let [col-name "column"
          board (-> (default-board)
                    (add-column col-name 0))
          work-item (register-new-work-item "work-item")
          column (column-with-name board col-name)]
      (is (thrown? Exception
                   (schedule-work-item board
                                       (:entity-id column)
                                       (:entity-id @work-item)))))))

(deftest advance-work-item-generates-message
  (testing "advance-work-item generateds the correct message."
    (let [board (-> (default-board)
                    (add-column "col-a" 10)
                    (add-column "col-b" 10))
          col-a ((:columns @board) 0)
          work-item (register-new-work-item "work-item")
          board (schedule-work-item board (:entity-id col-a) (:entity-id @work-item))
          handler (fn [{data :data}]
                    (is (= (:work-item-id data) (:entity-id @work-item))))]
      (with-subscribe handler
        (advance-work-item board (:entity-id @work-item))))))

(deftest advance-work-item-advances-work-item
  (testing "advance-work-item advances work item."
    (let [board (-> (default-board)
                    (add-column "col-a" 10)
                    (add-column "col-b" 10))
          col-a ((:columns @board) 0)
          col-b ((:columns @board) 1)
          work-item (register-new-work-item "work-item")
          work-item-id (:entity-id @work-item)]

      (schedule-work-item board (:entity-id col-a) work-item-id)
      (let [col-a (column-with-id board (:entity-id col-a))
            col-b (column-with-id board (:entity-id col-b))]
        (is (in? (:cards col-a) work-item-id))
        (is (not (in? (:cards col-b) work-item-id))))
      
      (advance-work-item board work-item-id)
      (let [col-a (column-with-id board (:entity-id col-a))
            col-b (column-with-id board (:entity-id col-b))]
        (is (not (in? (:cards col-a) work-item-id)))
        (is (in? (:cards col-b) work-item-id)))))
  )

  
;;                                         ; advance-work-item-throws-on-wip-limit-violation
;;                                         ; advance-work-item-throws-on-no-more-columns
  
