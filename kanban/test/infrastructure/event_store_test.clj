(ns infrastructure.event-store-test
  (:require [clojure.java.io :as io]
            [clojure.data.json :as json]
            [clojure.test :refer :all]
            [infrastructure.event_store :refer :all]
            [utilities.file :refer :all]))

(def store-directory "events.json")

(defn db-cleanup-fixture [f]
  (if (.exists (io/file store-directory))
    (delete-recursively store-directory))
  (f)
  (if (.exists (io/file store-directory))
    (delete-recursively store-directory)))

(use-fixtures :each db-cleanup-fixture)

(deftest events-can-be-recovered
  (let [event-store (create-file-event-store store-directory
                                             json/write-str
                                             json/read-str)
        events [1 2 3 4 5 1 2 3 4 5]]
    (doall (map (partial store event-store) events))
    (is (= (vec (.events event-store)) events))))


