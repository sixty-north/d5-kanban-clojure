(ns kanban.model.work-item)

(use '[kanban.model.domain_events :only [apply-event create-event process-event]])
(use '[kanban.model.entity :only [create-entity]])
(use '[utilities.uuid :only [uuid]])

(def work-item-entity-type :work-item)

(defn create-work-item-event
  "Create an event with `work-item-entity-type` as the entity-type."
  [event-type root-id & [data]]
  (create-event event-type work-item-entity-type root-id data))

(defprotocol Repository
  "Interface for repositories that produce `WorkItem` instances."
  (all-work-items [this]))

(defn- check-name-constraint [name]
  (if (empty? name)
    (throw (IllegalArgumentException.
            "A work-item's name can not be empty."))))

(defn register-new-work-item
  "Create a new work-item.

Throws `IllegalArgumentException` if `name` is empty.
"
  [name
   & {:keys [due-date content]
      :or {due-date nil
           content nil}}]
  (check-name-constraint name)
  (let [work-item-id (uuid)
        event (create-work-item-event :work-item-created
               work-item-id
               {:work-item-id work-item-id
                :name name
                :due-date due-date
                :content content})]
    (process-event event)))

(defmethod apply-event
  :work-item-created
  [nil-state
   {{name :name
     due-date :due-date
     content :content
     work-item-id :work-item-id} :data :as event}]
  (assert (= nil-state nil))
  (create-entity :work-item
                 work-item-id
                 0
                 {:name name
                  :due-date due-date
                  :content content}))

(defn rename-work-item
  "Changes the name of a work-item.

Throws `IllegalValueException` if `name` is empty.
"
  [work-item name]

  (check-name-constraint name)
  
  (let [event (create-work-item-event :work-item-renamed
                            (:entity-id @work-item)
                            {:name name})]
    (process-event event work-item)))

(defmethod apply-event
  :work-item-renamed
  [work-item
   {{name :name} :data}]
  (assoc work-item :name name))

(defn set-work-item-due-date
  "Changes the due-date of a work-item."
  [work-item due-date]
  (let [event (create-work-item-event :work-item-due-date-set
                            (:entity-id @work-item)
                            {:date due-date})]
    (process-event event work-item)))

(defmethod apply-event
  :work-item-due-date-set
  [work-item
   {{date :date} :data}]
  (assoc work-item :due-date date))

(defn set-work-item-content
  "Changes the content of a work-item."
  [work-item content]
  (let [event (create-work-item-event :work-item-content-set
                            (:entity-id @work-item)
                            {:content content})]
    (process-event event work-item)))

(defmethod apply-event
  :work-item-content-set
  [work-item
   {{content :content} :data}]
  (assoc work-item :content content))


