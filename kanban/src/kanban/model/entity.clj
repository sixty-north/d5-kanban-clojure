(ns kanban.model.entity)

(defn create-entity [type id version & [data]]
  "Create an entity map with the specified data.

The mapping is:
```
{:entity-type type
 :entity-id id
 :entity-version version
 <DATA>}
```

Note that any keys in `data` which collide with the required entity
keys are silently lost.

This returns the new Entity (not a ref.)
"
                                        ; TODO: Check for collision. That should be disallowed, I guess.

  (conj data {:entity-type type
              :entity-version version
              :entity-id id}))
