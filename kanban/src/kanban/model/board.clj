(ns kanban.model.board)

(use '[kanban.model.domain_events :only [apply-event create-event process-event]])
(use '[kanban.model.entity :only [create-entity]])
(use 'utilities.data_structures)
(use '[utilities.uuid :only [uuid]])

(defprotocol Repository
  "Interface for repositories that produce `Board` instances."

  (all-boards [this]
    "Return a sequence of refs to all `Boards` in the repository."))

; Tag used to mark events for Board entities.
(def board-entity-type :board)

(defn create-board-event
  "Create an event with `board-entity-type` as the entity-type."
  [event-type root-id & [data]]
  (create-event event-type board-entity-type root-id data))

;; Board shape
;;
;; {:name name
;;  :description description
;;  :columns [Column . . .]}

(defn- update-attr [board event attr-name]
  "Extract `:<attr-name>` from the event data and set
`:<attr-name>` on `board` to that value."
  (let [attr-key (keyword attr-name)
        new-val (-> event :data attr-key)]
    (assoc board attr-key new-val)))

;; api

(defn start-project 
  "Create a new `Board`.

Returns the new `Board` ref."  
  [name description]
  (let [board-id (uuid)
        event (create-board-event :project-started
                                  board-id
                                  {:board-id board-id
                                   :name name
                                   :description description})]
    (process-event event)))

(defmethod apply-event 
  :project-started
  [nil-state
   {{name :name
     description :description
     board-id :board-id} :data}]
  (assert (nil? nil-state))
  (create-entity board-entity-type
                 board-id
                 0
                 {:name name
                  :description description
                  :columns []}))

(defn discard-board
  "Discard `board`. This returns `nil`, i.e. the new state for the
  board aggregate."
  [board]
  (let [event (create-board-event :board-discarded
                                  (:entity-id @board))]
    (process-event event board)))

(defmethod apply-event
  :board-discarded
  [board event]
  nil)

(defn rename-board
  "Change the name of a board."
  [board new-name]
  (let [event (create-board-event :board-renamed
                                  (:entity-id @board)
                                  {:name new-name})]
    (process-event event board)))

(defmethod apply-event
  :board-renamed
  [board event]
  (update-attr board event "name"))

(defn change-board-description
  "Change the description of a board."
  [board new-description]
  (let [event (create-board-event :board-description-changed
                                  (:entity-id @board)
                                  {:description new-description})]
    (process-event event board)))

(defmethod apply-event
  :board-description-changed
  [board event]
  (update-attr board event "description"))

(defn- create-column
  "Utility for creating a new column entity."
  [column-id board-id name wip-limit]
  (create-entity :column column-id 0
                 {:board-id board-id
                  :name name
                  :wip-limit wip-limit
                  :cards []}))

(defn add-column 
  "Add a column named `col-name` to `board`. Returns `board` updated
  with the new column."  
  [board col-name wip-limit]
  ; TODO: Check for column name duplicates?

  ; TODO: Figure out better exception type.
  (if (< wip-limit 0)
    (throw (IllegalArgumentException. "wip-limit for columns must be non-negative.")))
  
  (let [event (create-board-event :column-added
                                  (:entity-id @board)
                                  {:name col-name
                                   :wip-limit wip-limit
                                   :column-id (uuid)})]
    (process-event event board)))

(defmethod apply-event 
  :column-added
  [board
   {{column-id :column-id
     name :name
     wip-limit :wip-limit} :data}]
  (let [column (create-column column-id
                              (:entity-id board)
                              name
                              wip-limit)
        columns (conj (:columns board) column)]
    (assoc board :columns columns)))

(defn insert-column
  "Insert a new column in a board at position `index`.

Raises `IndexOutOfBoundsException` if `index > num-columns`. 
"
  [board index col-name wip-limit]
  (if (< wip-limit 0)
    (throw (IllegalArgumentException. "wip-limit for columns must be non-negative.")))

  (let [event (create-board-event :column-inserted
                                  (:entity-id @board)
                                  {:index index
                                   :name col-name
                                   :wip-limit wip-limit
                             :column-id (uuid)})]
    (process-event event board)))

(defmethod apply-event
  :column-inserted
  [board
   {{column-id :column-id
     index :index
     name :name
     wip-limit :wip-limit} :data}]
  (let [column (create-column column-id
                              (:entity-id board)
                              name
                              wip-limit)
        columns (insert-into-vec (:columns board) index column)]
    (assoc board :columns columns)))

(defn remove-column [board column-id]
  (let [event (create-board-event :column-removed
                                  (:entity-id @board)
                                  {:column-id column-id})]
    (process-event event board)))

(defmethod apply-event
  :column-removed
  [board
   {{column-id :column-id} :data}]
  (let [match-col-id (fn [c] (= (:entity-id c) column-id))
        columns (vector (remove match-col-id (:columns board)))]
    (assoc board :columns columns)))

(defn column-with-name [board name]
  ; TODO: This is predicated on the constraints that columns must have
  ; unique names. Is that decided? Enforced?
  (first (filter #(= (:name %) name) (:columns @board))))

(defn column-with-id [board column-id]
  "Find a column in `board` with `column-id`."
  (first (filter #(= (:entity-id %) column-id) (:columns @board))))

(defn number-of-work-items [column]
  "Get the number of work-items assigned to `column`."
  (count (:cards column)))

(defn can-accept-work-item? [column]
  "Determine if `column` can accept any more work items."
  (< (number-of-work-items column) (:wip-limit column)))

(defn- add-card [column card-id]
  (if (not (can-accept-work-item? column))
          ; TODO: a better exception type?
          (throw (Exception. "wip-limit already reached in column.")))
  
  (let [cards (conj (:cards column) card-id)]
    (assoc column :cards cards)))

(defn- remove-card [column card-id]
  (let [cards (apply vector (remove #{card-id} (:cards column)))]
    (assoc column :cards cards)))

(defn- column-index [column-id board]
  (first (indices #(= (:entity-id %) column-id) (:columns board))))

;; ;; (defn- update-column [board column-id f]
;; ;;   (let [col-idx (column-index column-id board)
;; ;;         column (f (get-entity column-id))]
    
    
;; ;;     (assoc board :columns columns)))

; TODO: A way to get from column to board might make this API nicer,
; i.e. only requiring column, not board and column-id. A simple
; reference from column to board would likely to do the trick.
(defn schedule-work-item [board column-id work-item-id]
  "Shedule a work-item with `work-item-id` to the column with
  `column-id`."
  (let [event (create-board-event :work-item-scheduled
                                  (:entity-id @board)
                                  {:work-item-id work-item-id
                                   :column-id column-id})]
    (process-event event board)))

(defmethod apply-event
  :work-item-scheduled
  [board
   {{column-id :column-id
     work-item-id :work-item-id} :data}]
  (let [column-idx (column-index column-id board)
        column (-> (nth (:columns board) column-idx)
                   (add-card work-item-id))]
    (->> (assoc (:columns board) column-idx column)
         (assoc board :columns))))

(defn advance-work-item [board work-item-id]
  "Move a work-item to the next column in a board."
  (let [event (create-board-event :work-item-advanced
                                  (:entity-id @board)
                                  {:work-item-id work-item-id})]
    (process-event event board)))

(defn- column-with-work-item [board work-item-id]
  (let [columns (:columns board)]
    (first (indices (fn [c] (in? (:cards c) work-item-id)) columns))))

(defmethod apply-event
  :work-item-advanced
  [board
   {{work-item-id :work-item-id} :data}]
  (let [column-idx (column-with-work-item board work-item-id)
        column ((:columns board) column-idx)
        column (remove-card column work-item-id)
        next-column-idx (inc column-idx)
        next-column ((:columns board) next-column-idx)
        next-column (add-card next-column work-item-id)]
    (->>
     (assoc (:columns board)
       column-idx column
       next-column-idx next-column)
     (assoc board :columns))))

;; ;; (defmethod apply-event
;; ;;   :work-item-advanced
;; ;;   [board
;; ;;    {{work-item-id :work-item-id} :data}]
  
;; ;;   (let [work-item (get-entity work-item-id)
;; ;;         curr-col-id (:column-id work-item)
;; ;;         next-col-id (nth (drop-while #(not= (:entity-id %) curr-col-id)
;; ;;                                      (:columns board)) 1)]
;; ;;     (-> board
;; ;;         (update-column curr-col-id #(remove-card work-item-id %))
;; ;;         (update-column next-col-id #(add-card work-item-id %)))))

;; ;;                                         ; remove-column-by-name
;; ;;                                         ; rename-column
;; ;;                                         ; set-wip-limit
;; ;;                                         ; # work-items in a columna
;; ;;                                         ; work-item-ids
;; ;;                                         ; column-names
;; ;;                                         ; columns
;; ;;                                         ; __contains__
;; ;;                                         ; abandon-work-item
;; ;;                                         ; retire-work-item
;; ;;                                         ; lead-time projection
;; ;;                                         ; overdue service
