(ns kanban.model.domain_events)

(use 'kanban.model.entity)
(use 'utilities.uuid)

(defmulti apply-event
  "Take an initial state (an aggregate root Entity - not a ref) and
  apply an event to it, returning the new state.

  Implementations of this method should do constraint enforcement,
  validation, etc. for those constraints that depend on the state of
  the Entity. This method is called inside a transaction and will be
  retried with the up-to-date version of the aggregate root on each
  pass.
"
  (fn [state event] (:event-type event)))

(defn create-event [event-type entity-type root-id & [data]]
  "Creat a new event with a unique ID and the specified data."
  {:event-type event-type
   :entity-type entity-type
   :event-id (uuid)
   :root-id root-id
   :data data})

(def subscribers (atom (hash-set)))

(defn subscribe [subscriber]
  (swap! subscribers conj subscriber))

(defn unsubscribe [subscriber]
  (swap! subscribers disj subscriber))

(defn publish [event]
  "Call each of the subscribers with `event` as the argument."
  (doall (map #(% event) @subscribers)))

(defn- process-event- [event root-ref]
  "Call `apply-event` with the dereferenced `root-ref` and `event`. If
this succeeds (i.e. it doesn't find any failed constraints, etc.)
then `root-ref` is updated with the value returned by `apply-event`
and the event is published. Returns `root-ref`.

This is the heart of the event-processing system. The entire function
is run inside a transaction, meaning that both `apply-event` and
`publish` occur in the transaction. So if multiple events are being
processed in parallel, the STM system will ensure that constraints (as
applied by `apply-event`) will be properly maintained in order.

Likewise, any messages sent to agents by event subscribers will only
be sent when the transaction succeeds. This is useful for e.g. storing
events.
"
  (dosync
   (alter root-ref apply-event event)
   (publish event))
  (assert (or (nil? @root-ref)
              (= (:root-id event) (:entity-id @root-ref))))
  root-ref)

(defn process-event
  ([event]
     "Process an event that doesn't have an existing aggregate
     root. This is typically for events that signify the creation of a
     new aggregate root."
     (process-event- event (ref nil)))
  ([event root-ref]
     "Process an event on an aggregate root (ref). The root may not be
     the actual entity upon which the event acts, but the root is
     needed in order to ensure transactional integrity for the
     change."
     (assert (= (:root-id event) (:entity-id @root-ref)))
     (process-event- event root-ref))  )
