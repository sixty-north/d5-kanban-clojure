(ns infrastructure.event_serialization)

(require '[clojure.data.json :as json])

(defn to-storage-format [event]
  "Converts an event object into its serialized form."
  (json/write-str event))

(defn from-storage-format [s]
  "Converts the serialized form of an event into a new event object."
  (let [event (json/read-str s :key-fn keyword)]
    (assoc event
      :event-type (keyword (:event-type event))
      :entity-type (keyword (:entity-type event)))))
