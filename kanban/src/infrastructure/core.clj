(ns infrastructure.core)

(use 'infrastructure.event_serialization)
(use 'infrastructure.event_store)
(use 'infrastructure.event_sourced_repos.board_repository)
(use 'kanban.model.board)
(use 'kanban.model.domain_events)

(require '[clojure.java.io :as io])
(require 'clojure.pprint)

(defn- make-event-store [filename]
  "Convenience function for creating a new event-store on `filename`
  after potentially deleting `filename`."
  (if (.exists (io/file filename))
    (io/delete-file filename))
  (create-file-event-store filename
                           to-storage-format 
                           from-storage-format))

(def event-store (agent (make-event-store "events.json")))

(defn store-event [event-store event]
  (store event-store event)
  event-store)

(defn trace-event [event] (println "TRACE EVENTS: " event))

(defn -main [& args]  
  ; The idea here is that the event will only be stored in the
  ; EventStore if the publication (which may be happenining in a
  ; transaction) completes. And the event should only get stored once.
  (subscribe #(send-off event-store store-event %))
  (subscribe trace-event)

  ; Create a board with TODO, in-progress, and done columns
  (force
   (let [board (start-project
                "test-board-1"
                "The first test board.")]
     (reduce
      (fn [board [name wip-limit]]
        (add-column board name wip-limit))
      board
      [["TODO" 3]
       ["In-progress" 45]
       ["done" 1]])))
    
  ; Create a board with greek-letter columns
  (force
   (let [board (start-project "test-board-2"
                              "The second test board.")
         board (reduce
                (fn [board [name wip-limit]]
                  (add-column board name wip-limit))
                board
                [["alpha" 1]
                 ["beta" 2]
                 ["gamma" 0]
                 ["epsilon" 3]
                 ["omega" 4]])]
     (discard-board board)))

  ; Reconstruct all of these from the event store.
  (let [board-repo (->BoardRepository @event-store)]
    (clojure.pprint/pprint (.all-boards board-repo)))

  (shutdown-agents))

