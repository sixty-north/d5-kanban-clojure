(ns infrastructure.event_sourced_repos.common)

(use 'kanban.model.domain_events)

(defn- update-entity-map [entities {:keys [root-id] :as event}]
  (->> 
   (apply-event (entities root-id) event)
   (assoc entities root-id)))

(defn all-entities [event-store entity-type-tag]
  "Process all events in `event-store` pertaining to `entity-type-tag`
  and return a sequences of refs to the Entities resulting from that
  processing."
  (let [correct-entity-type? (fn [{entity-type :entity-type}] (= entity-type entity-type-tag))
        events (filter correct-entity-type? (.events event-store))]
    (->>
     (reduce update-entity-map {} events)
     vals
     (filter #(not (nil? %)))
     (map ref))))
