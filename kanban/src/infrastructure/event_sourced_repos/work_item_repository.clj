(ns infrastructure.event_sourced_repos.work_item_repository)

(use 'infrastructure.event_sourced_repos.common)
(use 'kanban.model.work-item)

(defrecord WorkItemRepository [event-store]
  kanban.model.work-item.Repository
  "An event-store-based Repository for work-items."
  (all-work-items [this]
    (all-entities (.event-store this) work-item-entity-type)))
