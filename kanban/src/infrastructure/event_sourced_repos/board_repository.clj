(ns infrastructure.event_sourced_repos.board_repository)

(use 'infrastructure.event_sourced_repos.common)
(use 'kanban.model.board)

(defrecord BoardRepository [event-store]
  kanban.model.board.Repository
  (all-boards [this]
    (all-entities (.event-store this) board-entity-type)))

