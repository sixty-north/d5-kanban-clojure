(ns infrastructure.event_store)

(require '[clojure.java.io :as io])

(defprotocol EventStore
  (store [this event])
  (events [this]))

(defrecord FileEventStore [filename
                           to-string 
                           from-string]
  EventStore
  
  (store [this event]
    (with-open [w (io/writer (.filename this) :append true)]
      (.write w ((.to-string this) event))
      (.write w "\n")))

  (events [this]
    (with-open [r (io/reader (.filename this))]
      (apply vector (map (.from-string this) (line-seq r))))))

(defn create-file-event-store [filename to-string from-string]
  (if (.exists (io/file filename))
    (throw (IllegalArgumentException.
            (format "File already exists: %s" filename))))

  (->FileEventStore filename to-string from-string))

; TODO: open-event-store, for accessing existing event stores
