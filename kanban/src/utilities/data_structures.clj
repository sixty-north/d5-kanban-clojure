(ns utilities.data_structures)

(defn insert-into-vec
  "Insert `x` into `vec` at position `pos`."
  [vec pos x]
  (let [len-orig (count vec)
        front (subvec vec 0 pos)
        tail (subvec vec pos)
        vec (apply conj front x tail)]
    (assert (= (inc len-orig) (count vec)))
    (assert (= (nth vec pos) x))
    vec))

(defn in? 
  "true if seq contains elm"
  [seq elm]  
  (some #(= elm %) seq))

(defn indices [pred coll]
  (keep-indexed (fn [idx x] (if (pred x) idx)) coll))
