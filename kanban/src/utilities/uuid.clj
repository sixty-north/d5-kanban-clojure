(ns utilities.uuid)

(defn uuid 
  "Generate a random UUID as a string."
  [] 
  (str (java.util.UUID/randomUUID)))
